package com.example.tp3and;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    private City city;
    private WeatherDbHelper DbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);



        DbHelper = new WeatherDbHelper(this);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);


        final Button butAdd = (Button) findViewById(R.id.button);

        butAdd.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {


                city = new City(textName.getText().toString(),textCountry.getText().toString());
                DbHelper.addCity(city);
                Intent intent = new Intent(NewCityActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }


}
